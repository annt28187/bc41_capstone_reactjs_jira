import { call, put, takeLatest } from 'redux-saga/effects';
import { statusService } from '../../../services/StatusSerivce';
import * as actionTypes from '../../constants/constants';

export function* getAllStatusSaga() {
  yield takeLatest(actionTypes.GET_ALL_STATUS, function* allStatus({ type }) {
    try {
      const res = yield call(() => statusService.fetchAllStatus());
      yield put({
        type: actionTypes.ALL_STATUS,
        payload: res.data.content,
      });
    } catch (err) {
      console.log(err.response);
    }
  });
  yield takeLatest(actionTypes.UPDATE_STATUS_DRAG, function* updateStatusDrag({ type, payload }) {
    try {
      const res = yield call(() => statusService.updateStatus(payload));
      yield put({
        type: actionTypes.GET_PROJECT_DETAIL_API,
        id: payload.id,
      });
      // console.log(payload.id)
    } catch (err) {
      console.log(err.response);
    }
  });
}
