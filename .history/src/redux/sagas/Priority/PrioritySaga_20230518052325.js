import axios from 'axios';
import { call, put, takeLatest } from 'redux-saga/effects';
import { priorityService } from '../../../services/PriorityService';
import * as actionTypes from '../../constants/constants';

export function* getAllPriority() {
  yield takeLatest(actionTypes.GET_ALL_PRIORITY, function* allPriority({ type, id }) {
    try {
      const res = yield call(() => priorityService.fetchPriority(id));
      yield put({
        type: actionTypes.ALL_PRIORITY,
        payload: res.data.content,
      });
    } catch (err) {
      console.log(err.response);
    }
  });
}
