import * as yup from 'yup';
const passwordRegex = /^(?=.*[A-Za-z])(?=.*\d)[A-Za-z\d]{8,}$/;
export const passWordSchema = yup.object().shape({
  passWord: yup
    .min(6, 'Password must be a number between 6-10 characters')
    .matches(passwordRegex, { message: 'Password must be a number between 6-10 characters' })
    .required('Please enter your password'),
  confirmPassword: yup
    .string()
    .required('Please confirm your password')
    .oneOf([yup.ref('passWord'), null], `Password doesn't match`)
    .required('* Please enter a new password'),
});
