import React from 'react';

const BreadCrumd = (props) => {
  return <div className={`text-sm text-gray-500 mb-2`}>{props.children}</div>;
};

export default BreadCrumd;
