import { CheckOutlined } from '@ant-design/icons';
import React from 'react';
import { Link, NavLink } from 'react-router-dom';
import { Header } from '../components/Home';

const Home = () => {
  return (
    <div>
      <Header />
      {/* linear bg */}
      <section className="bg-gradient-to-l from-blue-700 via-blue-800 to-gray-900 py-24 mt-12">
        <div className="max-w-[880px] mx-auto md:flex justify-between  gap-20 text-white">
          <div className="md:w-1/2 px-3 ">
            <h3 className="text-4xl font-semibold ">
              The #1 software development tool used by agile teams
            </h3>
            <p className="text-md font-bold mt-7 mb-5">EACH PRODUCT ON A FREE PLAN:</p>
            <ul>
              <li className="flex items-center gap-4 my-2">
                <CheckOutlined /> Supports up to 10 users
              </li>
              <li className="flex items-center gap-4 my-2">
                <CheckOutlined /> Includes 2 GB storage
              </li>
              <li className="flex items-center gap-4 my-2">
                <CheckOutlined /> Offers Community support
              </li>
              <li className="flex items-center gap-4 my-2">
                <CheckOutlined /> Is always free, no credit card needed
              </li>
            </ul>
          </div>
          <div className="md:w-1/2 pt-10">
            <div className="bg-white rounded shadow-lg text-black px-5 py-8 text-center dark:bg-secondary-dark dark:text-white">
              <h3 className="text-4xl font-semibold ">Get started</h3>
              <p className="mt-1 mb-4"> Free for up to 10 users</p>
              <NavLink
                to="/signup"
                className="self-center px-5 py-2 font-semibold rounded border border-blue-600 text-blue-600 hover:bg-blue-600 hover:text-white duration-200"
              >
                SIGN UP NOW
              </NavLink>
              <div className="mt-6 ">
                <svg
                  className="h-4  mx-auto"
                  xmlns="http://www.w3.org/2000/svg"
                  xmlnsXlink="http://www.w3.org/1999/xlink"
                  viewBox="0 0 532.119 66.025"
                >
                  <defs>
                    <style
                      dangerouslySetInnerHTML={{
                        __html:
                          '.cls-1{fill:url(#linear-gradient);}.cls-2{fill:#2684ff;}.cls-3{fill:#0052cc;}',
                      }}
                    />
                    <linearGradient
                      id="linear-gradient"
                      x1="28.121"
                      y1="35.051"
                      x2="11.239"
                      y2="64.292"
                      gradientUnits="userSpaceOnUse"
                    >
                      <stop offset={0} stopColor="#0052cc" />
                      <stop offset="0.923" stopColor="#2684ff" />
                    </linearGradient>
                  </defs>
                  <title>Atlassian-horizontal-blue-rgb</title>
                  <g id="Layer_2" data-name="Layer 2">
                    <g id="Blue">
                      <path
                        className="cls-1"
                        d="M19.354,30.115a1.856,1.856,0,0,0-3.157.343L.2,62.442a1.912,1.912,0,0,0,1.71,2.767H24.185a1.843,1.843,0,0,0,1.71-1.057C30.7,54.223,27.788,39.126,19.354,30.115Z"
                      />
                      <path
                        className="cls-2"
                        d="M31.087,1.024a42.188,42.188,0,0,0-2.463,41.65L39.363,64.152a1.912,1.912,0,0,0,1.71,1.057H63.344a1.912,1.912,0,0,0,1.71-2.767S35.092,2.511,34.339,1.012A1.806,1.806,0,0,0,31.087,1.024Z"
                      />
                      <path
                        className="cls-3"
                        d="M292.314,26.669c0,7.92,3.674,14.208,18.045,16.984,8.574,1.8,10.37,3.184,10.37,6.042,0,2.776-1.8,4.573-7.839,4.573a44.236,44.236,0,0,1-20.821-5.634v12.9c4.328,2.123,10.043,4.491,20.658,4.491,15.024,0,20.985-6.7,20.985-16.657m0,0c0-9.39-4.981-13.8-19.025-16.82-7.757-1.715-9.635-3.429-9.635-5.879,0-3.1,2.776-4.409,7.92-4.409,6.206,0,12.33,1.878,18.127,4.491V14.421a40.97,40.97,0,0,0-17.719-3.674c-13.881,0-21.066,6.042-21.066,15.922"
                      />
                      <polygon
                        className="cls-3"
                        points="485.332 11.563 485.332 65.209 496.763 65.209 496.763 24.301 501.581 35.16 517.748 65.209 532.119 65.209 532.119 11.563 520.688 11.563 520.688 46.184 516.36 36.14 503.377 11.563 485.332 11.563"
                      />
                      <rect
                        className="cls-3"
                        x="400.133"
                        y="11.563"
                        width="12.493"
                        height="53.646"
                      />
                      <path
                        className="cls-3"
                        d="M385.718,49.368c0-9.39-4.981-13.8-19.025-16.82-7.757-1.715-9.635-3.429-9.635-5.879,0-3.1,2.776-4.409,7.92-4.409,6.206,0,12.33,1.878,18.127,4.491V14.421a40.97,40.97,0,0,0-17.719-3.674c-13.881,0-21.066,6.042-21.066,15.922,0,7.92,3.674,14.208,18.045,16.984,8.574,1.8,10.37,3.184,10.37,6.042,0,2.776-1.8,4.573-7.839,4.573a44.236,44.236,0,0,1-20.821-5.634v12.9c4.328,2.123,10.043,4.491,20.658,4.491,15.024,0,20.985-6.7,20.985-16.657"
                      />
                      <polygon
                        className="cls-3"
                        points="195.265 11.563 195.265 65.209 220.943 65.209 224.986 53.614 207.839 53.614 207.839 11.563 195.265 11.563"
                      />
                      <polygon
                        className="cls-3"
                        points="144.533 11.563 144.533 23.157 158.414 23.157 158.414 65.209 170.988 65.209 170.988 23.157 185.849 23.157 185.849 11.563 144.533 11.563"
                      />
                      <path
                        className="cls-3"
                        d="M126.3,11.563H109.821L91.114,65.209H105.4l2.652-9.035a35.508,35.508,0,0,0,20.008,0l2.652,9.035h14.289ZM118.06,46.5a24.4,24.4,0,0,1-6.875-.989L118.06,22.1l6.875,23.419A24.4,24.4,0,0,1,118.06,46.5Z"
                      />
                      <path
                        className="cls-3"
                        d="M265.211,11.563H248.733L230.026,65.209h14.289l2.652-9.035a35.508,35.508,0,0,0,20.008,0l2.652,9.035h14.289ZM256.972,46.5a24.4,24.4,0,0,1-6.875-.989L256.972,22.1l6.875,23.419A24.4,24.4,0,0,1,256.972,46.5Z"
                      />
                      <path
                        className="cls-3"
                        d="M457.644,11.563H441.166L422.459,65.209h14.289l2.652-9.035a35.508,35.508,0,0,0,20.008,0l2.652,9.035H476.35ZM449.4,46.5a24.4,24.4,0,0,1-6.875-.989L449.4,22.1l6.875,23.419A24.4,24.4,0,0,1,449.4,46.5Z"
                      />
                    </g>
                  </g>
                </svg>
              </div>
            </div>
          </div>
        </div>
      </section>
      {/* introduce */}
      <section className="text-center pt-10 bg-gradient-to-b from-white via-gray-100 to-gray-200  dark:bg-gradient-to-b dark:from-third-dark dark:via-secondary-dark dark:to-primary-dark">
        <h3 className="text-4xl font-semibold text-gray-500 dark:text-white ">Jira your way</h3>
        <p className="text-xl text-gray-500 py-6 ">
          Easily customize your workflows by turning on (or off) tons of powerful features with a
          single click.
        </p>
        <div className="max-w-[600px] mx-auto ">
          <img
            id="84435c58"
            alt="Screenshot of toggling features"
            className="component__image"
            src="https://wac-cdn.atlassian.com/dam/jcr:720d8ce3-ab0c-4eb9-bb9b-515ac674e160/01_Scrnshot_jira-configuration-static.png?cdnVersion=892"
            loading="auto"
          />
        </div>
      </section>
      <section className="shadow-lg dark:bg-secondary-dark pt-10">
        <div className="text-center max-w-[750px] mx-auto pb-10 ">
          <p className="text-xl text-gray-500 font-semibold">
            {' '}
            “We grew to 30 projects with lots of plans and branches under each, which means 50
            running builds at the same time. Considering the number of projects, we couldn’t have
            survived without Atlassian.”
          </p>
          <span className="text-slate-900 text-xs mt-4">
            Scott Carpenter Work Program Manager, Fugro Roames
          </span>
        </div>
      </section>
      <section className="dark:bg-third-dark dark:text-white">
        <div className="md:flex justify-between max-w-[900px] py-6 gap-14 items-center mx-auto px-2">
          <div className="md:w-1/3">
            <h3 className="font-bold text-2xl mb-2">Unfunk your workflow</h3>
            <p>Set up, clean up, and easily manage even the most hectic project workflows.</p>
          </div>
          <div className="md:w-2/3">
            <img
              id="fc934c13"
              alt="Screenshot of Next-gen board"
              className="component__image"
              src="https://wac-cdn.atlassian.com/dam/jcr:999ace66-3f06-4675-916b-c8dfaffc0795/Unfunk-JSW-PSD-2x.png?cdnVersion=892"
              loading="auto"
            />
          </div>
        </div>
        <div className="md:flex justify-between max-w-[900px] py-6 gap-14 items-center mx-auto px-2">
          <div className="md:w-2/3">
            <img
              id="c7a6c6d5"
              alt="Screenshot of Jira roadmap"
              className="component__image"
              src="https://wac-cdn.atlassian.com/dam/jcr:8f84e087-3d18-46a1-9453-ce370c45dbc1/Stay-JSW-PSD-2x.png?cdnVersion=892"
              loading="auto"
            />
          </div>
          <div className="md:w-1/3">
            <h3 className="font-bold text-2xl mb-2">Unfunk your workflow</h3>
            <p>Set up, clean up, and easily manage even the most hectic project workflows.</p>
          </div>
        </div>
        <div className="md:flex px-3 justify-between max-w-[900px] py-6 gap-14 items-center mx-auto">
          <div className="md:w-1/3">
            <h3 className="font-bold text-2xl mb-2">Unfunk your workflow</h3>
            <p>Set up, clean up, and easily manage even the most hectic project workflows.</p>
          </div>
          <div className="md:w-2/3">
            <img
              id="45e0c729"
              alt="Screenshot of Jira ticket"
              className="component__image"
              src="https://wac-cdn.atlassian.com/dam/jcr:7e2d0ac1-3631-4e1e-a5c5-4c106c323aee/Ditch-JSW-PSD-2x.png?cdnVersion=892"
              loading="auto"
            />
          </div>
        </div>
      </section>
      <section className="bg-gradient-to-l from-blue-700 via-blue-800 to-gray-900 py-12 pt-12">
        <div className="max-w-[880px] mx-auto text-center text-white">
          <h3 className="text-4xl font-semibold">Concept to launch in record time.</h3>
          <button className="bg-yellow-500 py-2 px-5 rounded text-black mt-6">Get it free</button>
        </div>
      </section>
      <section className="text-center py-14 dark:bg-third-dark dark:text-white">
        <div className=" max-w-[900px] mx-auto">
          <h3 className="text-2xl font-semibold"> Trusted by over 100,000 customers world-wide</h3>
          <div className="flex gap-6 justify-center mt-6 flex-col md:flex-row">
            <img
              id="e3be2884"
              alt="Square logo"
              className="component__image"
              style={{ height: 45 }}
              src="https://wac-cdn.atlassian.com/dam/jcr:4cba45db-e328-4abd-88ea-bfe276355cb5/Square%20Logo.svg?cdnVersion=892"
              loading="auto"
            />
            <img
              id="408adcd5"
              alt="ebay logo"
              className="component__image"
              style={{ height: 45 }}
              src="https://wac-cdn.atlassian.com/dam/jcr:db51d228-2145-498b-ab73-064aa651770d/ebay%20logo.svg?cdnVersion=892"
              loading="auto"
            />
            <img
              id="254ea3eb"
              alt="Spotify logo"
              className="component__image"
              style={{ height: 45 }}
              src="https://wac-cdn.atlassian.com/dam/jcr:7db3e103-186c-4413-950d-dea2f2a5755c/Spotify%20logo.svg?cdnVersion=892"
              loading="auto"
            />
            <img
              id="e3be2884"
              alt="Square logo"
              className="component__image"
              style={{ height: 45 }}
              src="https://wac-cdn.atlassian.com/dam/jcr:4cba45db-e328-4abd-88ea-bfe276355cb5/Square%20Logo.svg?cdnVersion=892"
              loading="auto"
            />
          </div>
        </div>
      </section>
      <footer className="container py-10 bg-gray-200 px-2 dark:bg-primary-dark dark:text-gray-300">
        <div className="max-w-[1000px] flex justify-around mx-auto ">
          <div>
            <ul>
              <h3 className="text-lg mb-3 font-semibold">PRODUCTS</h3>
              <li>Jira Software</li>
              <li>Jira Align</li>
              <li>Jira Service Management</li>
              <li>Confluence</li>
              <li>Trello</li>
              <li>Bitbucket</li>
            </ul>
          </div>
          <div>
            <ul>
              <h3 className="text-lg mb-3 font-semibold">RESOURCES</h3>
              <li>Technical Support</li>
              <li>Purchasing & licensing</li>
              <li>Atlassian Community</li>
              <li>Knowledge base</li>
              <li>Marketplace</li>
              <li>My Account</li>
            </ul>
          </div>
          <div>
            <ul>
              <h3 className="text-lg mb-3 font-semibold">EXPAND & LEARN</h3>
              <li>Partners</li>
              <li>Training & Certification</li>
              <li>Documentation</li>
              <li>Developer Resources</li>
              <li>Enterprise services</li>
            </ul>
          </div>
          <div>
            <ul>
              <h3 className="text-lg mb-3 font-semibold">ABOUT ATLASSIAN</h3>
              <li>Company</li>
              <li>Careers</li>
              <li>Events</li>
              <li>Blogs</li>
              <li>Atlassian Foundation</li>
              <li>Investor Relations</li>
              <li>Trust & Security</li>
            </ul>
          </div>
        </div>
        <div>
          <hr className="my-6 border-gray-200 sm:mx-auto dark:border-gray-700 lg:my-8" />
          <div className="container sm:flex sm:items-center sm:justify-between">
            <span className="text-sm text-gray-500 sm:text-center dark:text-gray-400">
              Copyright © 2023
              <link href="https://www.atlassian.com/" className="hover:underline" />
              Atlassian . All Rights Reserved.
            </span>
            <div className="flex mt-4 space-x-6 sm:justify-center sm:mt-0">
              <link href="#" className="text-gray-500 hover:text-gray-900 dark:hover:text-white" />
              <svg className="w-5 h-5" fill="currentColor" viewBox="0 0 24 24" aria-hidden="true">
                <path
                  fillRule="evenodd"
                  d="M22 12c0-5.523-4.477-10-10-10S2 6.477 2 12c0 4.991 3.657 9.128 8.438 9.878v-6.987h-2.54V12h2.54V9.797c0-2.506 1.492-3.89 3.777-3.89 1.094 0 2.238.195 2.238.195v2.46h-1.26c-1.243 0-1.63.771-1.63 1.562V12h2.773l-.443 2.89h-2.33v6.988C18.343 21.128 22 16.991 22 12z"
                  clipRule="evenodd"
                />
              </svg>
              <span className="sr-only">Facebook page</span>
              <link href="#" className="text-gray-500 hover:text-gray-900 dark:hover:text-white" />
              <svg className="w-5 h-5" fill="currentColor" viewBox="0 0 24 24" aria-hidden="true">
                <path
                  fillRule="evenodd"
                  d="M12.315 2c2.43 0 2.784.013 3.808.06 1.064.049 1.791.218 2.427.465a4.902 4.902 0 011.772 1.153 4.902 4.902 0 011.153 1.772c.247.636.416 1.363.465 2.427.048 1.067.06 1.407.06 4.123v.08c0 2.643-.012 2.987-.06 4.043-.049 1.064-.218 1.791-.465 2.427a4.902 4.902 0 01-1.153 1.772 4.902 4.902 0 01-1.772 1.153c-.636.247-1.363.416-2.427.465-1.067.048-1.407.06-4.123.06h-.08c-2.643 0-2.987-.012-4.043-.06-1.064-.049-1.791-.218-2.427-.465a4.902 4.902 0 01-1.772-1.153 4.902 4.902 0 01-1.153-1.772c-.247-.636-.416-1.363-.465-2.427-.047-1.024-.06-1.379-.06-3.808v-.63c0-2.43.013-2.784.06-3.808.049-1.064.218-1.791.465-2.427a4.902 4.902 0 011.153-1.772A4.902 4.902 0 015.45 2.525c.636-.247 1.363-.416 2.427-.465C8.901 2.013 9.256 2 11.685 2h.63zm-.081 1.802h-.468c-2.456 0-2.784.011-3.807.058-.975.045-1.504.207-1.857.344-.467.182-.8.398-1.15.748-.35.35-.566.683-.748 1.15-.137.353-.3.882-.344 1.857-.047 1.023-.058 1.351-.058 3.807v.468c0 2.456.011 2.784.058 3.807.045.975.207 1.504.344 1.857.182.466.399.8.748 1.15.35.35.683.566 1.15.748.353.137.882.3 1.857.344 1.054.048 1.37.058 4.041.058h.08c2.597 0 2.917-.01 3.96-.058.976-.045 1.505-.207 1.858-.344.466-.182.8-.398 1.15-.748.35-.35.566-.683.748-1.15.137-.353.3-.882.344-1.857.048-1.055.058-1.37.058-4.041v-.08c0-2.597-.01-2.917-.058-3.96-.045-.976-.207-1.505-.344-1.858a3.097 3.097 0 00-.748-1.15 3.098 3.098 0 00-1.15-.748c-.353-.137-.882-.3-1.857-.344-1.023-.047-1.351-.058-3.807-.058zM12 6.865a5.135 5.135 0 110 10.27 5.135 5.135 0 010-10.27zm0 1.802a3.333 3.333 0 100 6.666 3.333 3.333 0 000-6.666zm5.338-3.205a1.2 1.2 0 110 2.4 1.2 1.2 0 010-2.4z"
                  clipRule="evenodd"
                />
              </svg>
              <span className="sr-only">Instagram page</span>
              <link href="#" className="text-gray-500 hover:text-gray-900 dark:hover:text-white" />
              <svg className="w-5 h-5" fill="currentColor" viewBox="0 0 24 24" aria-hidden="true">
                <path d="M8.29 20.251c7.547 0 11.675-6.253 11.675-11.675 0-.178 0-.355-.012-.53A8.348 8.348 0 0022 5.92a8.19 8.19 0 01-2.357.646 4.118 4.118 0 001.804-2.27 8.224 8.224 0 01-2.605.996 4.107 4.107 0 00-6.993 3.743 11.65 11.65 0 01-8.457-4.287 4.106 4.106 0 001.27 5.477A4.072 4.072 0 012.8 9.713v.052a4.105 4.105 0 003.292 4.022 4.095 4.095 0 01-1.853.07 4.108 4.108 0 003.834 2.85A8.233 8.233 0 012 18.407a11.616 11.616 0 006.29 1.84" />
              </svg>
              <span className="sr-only">Twitter page</span>
              <link href="#" className="text-gray-500 hover:text-gray-900 dark:hover:text-white" />
              <svg className="w-5 h-5" fill="currentColor" viewBox="0 0 24 24" aria-hidden="true">
                <path
                  fillRule="evenodd"
                  d="M12 2C6.477 2 2 6.484 2 12.017c0 4.425 2.865 8.18 6.839 9.504.5.092.682-.217.682-.483 0-.237-.008-.868-.013-1.703-2.782.605-3.369-1.343-3.369-1.343-.454-1.158-1.11-1.466-1.11-1.466-.908-.62.069-.608.069-.608 1.003.07 1.531 1.032 1.531 1.032.892 1.53 2.341 1.088 2.91.832.092-.647.35-1.088.636-1.338-2.22-.253-4.555-1.113-4.555-4.951 0-1.093.39-1.988 1.029-2.688-.103-.253-.446-1.272.098-2.65 0 0 .84-.27 2.75 1.026A9.564 9.564 0 0112 6.844c.85.004 1.705.115 2.504.337 1.909-1.296 2.747-1.027 2.747-1.027.546 1.379.202 2.398.1 2.651.64.7 1.028 1.595 1.028 2.688 0 3.848-2.339 4.695-4.566 4.943.359.309.678.92.678 1.855 0 1.338-.012 2.419-.012 2.747 0 .268.18.58.688.482A10.019 10.019 0 0022 12.017C22 6.484 17.522 2 12 2z"
                  clipRule="evenodd"
                />
              </svg>
              <span className="sr-only">GitHub account</span>
            </div>
          </div>
        </div>
      </footer>
    </div>
  );
};

export default Home;
