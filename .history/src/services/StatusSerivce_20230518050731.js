import axios from 'axios';
import * as actionTypes from '../redux/constants/constants';
import { BASE_URL, TOKEN_CYBERSOFT } from './config';

export const statusService = {
  fetchAllStatus: () =>
    axios({
      url: `${BASE_URL}/Status/getAll`,
      method: 'GET',
      headers: {
        TokenCybersoft: TOKEN_CYBERSOFT,
      },
    }),
  updateStatus: (payload) =>
    axios({
      url: `${BASE_URL}/Project/updateStatus`,
      method: 'PUT',
      headers: {
        TokenCybersoft: TOKEN_CYBERSOFT,
        Authorization: 'Bearer ' + localStorage.getItem(actionTypes.USER_TOKEN),
      },
      data: payload,
    }),
};
