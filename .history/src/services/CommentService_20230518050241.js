import axios from 'axios';
import { BASE_URL, TOKEN_CYBERSOFT } from './config';

const HEADERS = {
  headers: {
    TokenCybersoft: TOKEN_CYBERSOFT,
    Authorization: 'Bearer ' + localStorage.getItem('USER_TOKEN'),
  },
};
export const commentService = {
  getAllComments: (taskId) =>
    axios({
      url: `${BASE_URL}/Comment/getAll`,
      method: 'GET',
      headers: {
        TokenCybersoft: TOKEN_CYBERSOFT,
      },
      params: {
        taskId: taskId,
      },
    }),
  postComments: (taskId) => axios.post(`${BASE_URL}/Comment/insertComment`, taskId, HEADERS),
  deleteComments: (Id) =>
    axios.delete(`${BASE_URL}/Comment/deleteComment?idComment=${Id}`, HEADERS),
  putComments: (Content, Id) =>
    axios.put(
      `${BASE_URL}/Comment/updateComment?id=${Id}&contentComment=${Content}`,
      null,
      HEADERS
    ),
};
