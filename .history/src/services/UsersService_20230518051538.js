import axios from 'axios';
import * as actionTypes from '../redux/constants/constants';
import { BASE_URL, TOKEN_CYBERSOFT } from './config';

export const usersService = {
  getUsers: (keyword) =>
    axios({
      url: `${BASE_URL}/Users/getUser`,
      method: 'GET',
      headers: {
        TokenCybersoft: TOKEN_CYBERSOFT,
        Authorization: 'Bearer ' + localStorage.getItem(actionTypes.USER_TOKEN),
      },
      params: {
        keyword,
      },
    }),
  getUserByIdProj: (idProject) =>
    axios({
      url: `${BASE_URL}/Users/getUserByProjectId`,
      method: 'GET',
      headers: {
        TokenCybersoft: TOKEN_CYBERSOFT,
        Authorization: 'Bearer ' + localStorage.getItem(actionTypes.USER_TOKEN),
      },
      params: {
        idProject,
      },
    }),
};
