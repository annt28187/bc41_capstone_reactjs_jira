import * as actionTypes from '../redux/constants/constants';
import axios from 'axios';
import { BASE_URL, TOKEN_CYBERSOFT } from './Config';

const HEADERS = {
  headers: {
    TokenCybersoft: TOKEN_CYBERSOFT,
  },
};
export const authService = {
  login: (data) => axios.post(`${BASE_URL}/Users/signin`, data, HEADERS),
  signUp: (data) =>
    axios({
      url: `${BASE_URL}/Users/signup`,
      method: 'POST',
      headers: {
        TokenCybersoft: TOKEN_CYBERSOFT,
      },
      data,
    }),
  testTokenAPI: () =>
    axios({
      url: `${BASE_URL}/Users/TestToken`,
      method: 'POST',
      headers: {
        TokenCybersoft: BASE_URL,
        Authorization: 'Bearer ' + localStorage.getItem(actionTypes.USER_TOKEN),
      },
    }),
};
