import axios from 'axios';
import * as actionType from '../redux/constants/constants';
import { BASE_URL, TOKEN_CYBERSOFT } from './config';

const HEADERS = {
  headers: {
    TokenCybersoft: TOKEN_CYBERSOFT,
    Authorization: 'Bearer ' + localStorage.getItem(actionType.USER_TOKEN),
  },
};
export const managerService = {
  listUser: () => axios.get(`${BASE_URL}/Users/getUser`, HEADERS),
  infoUser: (data) => axios.get(`${BASE_URL}/Users/getUser?keyword=${data}`, HEADERS),
  editUser: (data) => axios.put(`${BASE_URL}/Users/editUser`, data, HEADERS),
  deleteUser: (id) => axios.delete(`${BASE_URL}/Users/deleteUser?id=${id}`, HEADERS),
  createUser: (data) => axios.post(`${BASE_URL}/Users/signup`, data),
};
