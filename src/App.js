import { BrowserRouter, Route, Routes } from 'react-router-dom';
import NotFoundPage from './pages/NotFoundPage';
import { LoadingPage } from './components/Global';
import { useSelector } from 'react-redux';
import ButtonDarkMode from './components/Global/ButtonDarkMode';
import RouteComponent from './HOCs/AppRoute';
import { adminRoutes } from './routes/adminRoutes';

function App() {
  const { isLoading } = useSelector((state) => state.reducer);

  return (
    <BrowserRouter>
      {isLoading && <LoadingPage />}
      <Routes>
        {adminRoutes.map(
          ({ path, component: Component, isPrivate, isAuth, redirectPath, isAdmin }) => (
            <Route
              key={path}
              path={path}
              element={
                <RouteComponent
                  isPrivate={isPrivate}
                  isAdmin={isAdmin}
                  isAuth={isAuth}
                  Component={Component}
                  redirectPath={redirectPath}
                />
              }
            />
          )
        )}
        <Route path="*" element={<NotFoundPage />} />
      </Routes>
      <ButtonDarkMode />
    </BrowserRouter>
  );
}

export default App;
