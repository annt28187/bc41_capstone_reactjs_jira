import axios from 'axios';
import * as actionTypes from '../redux/constants/constants';
import { BASE_URL, TOKEN_CYBERSOFT } from './config';

export const projectService = {
  fetchProjectCategory: () =>
    axios({
      url: `${BASE_URL}/ProjectCategory`,
      method: 'GET',
      headers: {
        TokenCybersoft: TOKEN_CYBERSOFT,
      },
    }),
  createProject: (data) =>
    axios({
      url: `${BASE_URL}/Project/createProjectAuthorize`,
      method: 'POST',
      headers: {
        TokenCybersoft: TOKEN_CYBERSOFT,
        Authorization: 'Bearer ' + localStorage.getItem(actionTypes.USER_TOKEN),
      },
      data,
    }),
  getAllProject: (keyword) =>
    axios({
      url: `${BASE_URL}/Project/getAllProject`,
      method: 'GET',
      headers: {
        TokenCybersoft: TOKEN_CYBERSOFT,
      },
      params: {
        keyword,
      },
    }),
  getProjectDetail: (id) =>
    axios({
      url: `${BASE_URL}/Project/getProjectDetail`,
      method: 'GET',
      headers: {
        TokenCybersoft: TOKEN_CYBERSOFT,
        Authorization: 'Bearer ' + localStorage.getItem(actionTypes.USER_TOKEN),
      },
      params: {
        id,
      },
    }),
  updateProject: (payload) =>
    axios({
      url: `${BASE_URL}/Project/updateProject`,
      method: 'PUT',
      headers: {
        TokenCybersoft: TOKEN_CYBERSOFT,
        Authorization: 'Bearer ' + localStorage.getItem(actionTypes.USER_TOKEN),
      },
      params: {
        projectId: payload.id,
      },
      data: payload,
    }),
  deleteProject: (id) =>
    axios({
      url: `${BASE_URL}/Project/deleteProject`,
      method: 'DELETE',
      headers: {
        TokenCybersoft: TOKEN_CYBERSOFT,
        Authorization: 'Bearer ' + localStorage.getItem(actionTypes.USER_TOKEN),
      },
      params: {
        projectId: id,
      },
    }),
  getUserProject: (keyword) =>
    axios({
      url: `${BASE_URL}/Users/getUser`,
      method: 'GET',
      headers: {
        TokenCybersoft: TOKEN_CYBERSOFT,
        Authorization: 'Bearer ' + localStorage.getItem(actionTypes.USER_TOKEN),
      },
      params: {
        keyword,
      },
    }),
  assignUserProject: (payload) =>
    axios({
      url: `${BASE_URL}/Project/assignUserProject`,
      method: 'POST',
      headers: {
        TokenCybersoft: TOKEN_CYBERSOFT,
        Authorization: 'Bearer ' + localStorage.getItem(actionTypes.USER_TOKEN),
      },
      data: payload,
    }),
  removeUserFromPrj: (payload) =>
    axios({
      url: `${BASE_URL}/Project/removeUserFromProject`,
      method: 'POST',
      headers: {
        TokenCybersoft: TOKEN_CYBERSOFT,
        Authorization: 'Bearer ' + localStorage.getItem(actionTypes.USER_TOKEN),
      },
      data: payload,
    }),
  createTaskProj: (payload) =>
    axios({
      url: `${BASE_URL}/Project/createTask`,
      method: 'POST',
      headers: {
        TokenCybersoft: TOKEN_CYBERSOFT,
        Authorization: 'Bearer ' + localStorage.getItem(actionTypes.USER_TOKEN),
      },
      data: payload,
    }),
  getTaskDetail: (payload) =>
    axios({
      url: `${BASE_URL}/Project/getTaskDetail`,
      method: 'GET',
      headers: {
        TokenCybersoft: TOKEN_CYBERSOFT,
        Authorization: 'Bearer ' + localStorage.getItem(actionTypes.USER_TOKEN),
      },
      params: {
        taskId: payload,
      },
    }),
};
