import axios from 'axios';
import * as actionTypes from '../redux/constants/constants';
import { BASE_URL, TOKEN_CYBERSOFT } from './config';

export const taskService = {
  getAllTaskTypes: () =>
    axios({
      url: `${BASE_URL}/TaskType/getAll`,
      method: 'GET',
      headers: {
        TokenCybersoft: TOKEN_CYBERSOFT,
      },
    }),
  updateTaskDetail: (payload) =>
    axios({
      url: `${BASE_URL}/Project/updateTask`,
      method: 'POST',
      headers: {
        TokenCybersoft: TOKEN_CYBERSOFT,
        Authorization: 'Bearer ' + localStorage.getItem(actionTypes.USER_TOKEN),
      },
      data: payload,
    }),
  deleteTaskDetail: (TaskId) =>
    axios({
      url: `${BASE_URL}/Project/removeTask?taskId=${TaskId}`,
      method: 'DELETE',
      headers: {
        TokenCybersoft: TOKEN_CYBERSOFT,
        Authorization: 'Bearer ' + localStorage.getItem(actionTypes.USER_TOKEN),
      },
    }),
};
