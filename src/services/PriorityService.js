import axios from 'axios';
import { BASE_URL, TOKEN_CYBERSOFT } from './config';

export const priorityService = {
  fetchPriority: (id) =>
    axios({
      url: `${BASE_URL}/Priority/getAll`,
      method: 'GET',
      headers: {
        TokenCybersoft: TOKEN_CYBERSOFT,
      },
      params: {
        id,
      },
    }),
};
